var Game = function(width, height) {
    this.width = width;
    this.height = height;
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 1000 );
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(this.width, this.height);

    document.body.appendChild(this.renderer.domElement);

    var self = this;

    this.render = function() {
        requestAnimationFrame(self.render);
        self.renderer.render(self.scene, self.camera);
        //self.camera.rotation.x += 0.1;
    };

    this.start = function() {
        var geometry = new THREE.CubeGeometry(1,1,1);
        var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var cube = new THREE.Mesh( geometry, material );
        this.scene.add( cube );

        this.camera.position.z = 5;

        this.render();
    };
};

window.onload = function() {
    var game = new Game(window.innerWidth, window.innerHeight);
    game.start();
}